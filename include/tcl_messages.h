
#pragma once
/*

 This header was generated using mdsshr/gen_device.py
 To add new status messages modify: 
     tcl_messages.xml
 and then in mdsshr do:
     python gen_devices.py
*/

#define TclNORMAL               0x2a0009
#define TclFAILED_ESSENTIAL     0x2a0010
