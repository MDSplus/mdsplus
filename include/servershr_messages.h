
#pragma once
/*

 This header was generated using mdsshr/gen_device.py
 To add new status messages modify: 
     servershr_messages.xml
 and then in mdsshr do:
     python gen_devices.py
*/

#define ServerNOT_DISPATCHED       0xfe18008
#define ServerINVALID_DEPENDENCY   0xfe18012
#define ServerCANT_HAPPEN          0xfe1801a
#define ServerINVSHOT              0xfe18022
#define ServerABORT                0xfe18032
#define ServerPATH_DOWN            0xfe18042
