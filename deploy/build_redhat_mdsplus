#!/bin/bash
echo build_redhat_mdsplus invoked with args=$@
set -o verbose
set -e 
#
# Get the 3rd party software headers etc from github
#
wget -q -O - https://github.com/MDSplus/3rd-party-apis/archive/master.tar.gz | (cd /; tar zxf -)
#
# make the root to build into
#
mkdir -p /buildroot
#
# In the source directory build and install both 64-bit and 32-bit applications.
#
cd /buildsrc
./configure --prefix=/buildroot/usr/local/mdsplus \
            --exec_prefix=/buildroot/usr/local/mdsplus \
            --with-labview=/3rd-party-apis-master/labview \
            --with-jdk=$JDK_DIR \
            --with-idl=/3rd-party-apis-master/idl \
            --with-gsi=/usr:gcc64 \
            --bindir=/buildroot/usr/local/mdsplus/bin64 \
            --libdir=/buildroot/usr/local/mdsplus/lib64 \
            --host=x86-64-linux \
            --with-java_target=6 --with-java_bootclasspath=$(pwd)/rt.jar;
make clean > /dev/null
env LANG=en_US.UTF-8 make
make install
make clean >/dev/null;
./configure --prefix=/buildroot/usr/local/mdsplus \
            --exec_prefix=/buildroot/usr/local/mdsplus \
            --with-labview=/3rd-party-apis-master/labview \
            --with-jdk=$JDK_DIR \
            --with-idl=/3rd-party-apis-master/idl \
            --with-gsi=/usr:gcc32 \
            --bindir=/buildroot/usr/local/mdsplus/bin32 \
            --libdir=/buildroot/usr/local/mdsplus/lib32 \
            --host=i686-linux \
            --with-java_target=6 --with-java_bootclasspath=$(pwd)/rt.jar;
env LANG=en_US.UTF-8 make;
#
# Define MDSPLUS_VERSION environment variable which is used for the python
# setup.py
#
if [ -z "$BNAME" ]
then
  export MDSPLUS_VERSION=${MAJOR}.${MINOR}.${RELEASE}
else
  export MDSPLUS_VERSION=${BRANCH}-${MAJOR}.${MINOR}.${RELEASE}
fi
make install
make clean >/dev/null;
#
# Build the debug version of code for testing
# with valgrind. Currently the testing is done with python tests so
# only need 64-bit since the default python is 64-bit. If when we add
# other tests we may want to test both 64-bit and 32-bit.
#

if ( echo "$@" | grep vg > /dev/null )
then
    mkdir /dbgbuildroot
    ./configure --prefix=/dbgbuildroot/usr/local/mdsplus \
              --exec_prefix=/dbgbuildroot/usr/local/mdsplus \
              --with-labview=/3rd-party-apis-master/labview \
              --with-jdk=$JDK_DIR \
              --with-idl=/3rd-party-apis-master/idl \
              --with-gsi=/usr:gcc64 \
              --bindir=/dbgbuildroot/usr/local/mdsplus/bin64 \
              --libdir=/dbgbuildroot/usr/local/mdsplus/lib64 \
              --host=x86-64-linux \
              --with-java_target=6 --with-java_bootclasspath=$(pwd)/rt.jar CFLAGS="-g -O0" FFLAGS="-g -O0"
    make clean > /dev/null
    env LANG=en_US.UTF-8 make
    make install
fi

if (( "$#" ))
then
    echo Doing tests: $@
    export LIBDIR=lib64
    chmod a+x ./deploy/do_tests
    ./deploy/do_tests $@
fi

#
# IF the /installer directory is found then build a repository with the new rpm's in it.
#

if [ -d /installer ]
then
    echo "Building rpms"
    mkdir -p /buildroot/etc/yum.repos.d
    mkdir -p /buildroot/etc/pki/rpm-gpg/
    mkdir -p /installer/BUILD /installer/BUILDROOT /installer/SPECS /installer/RPMS /installer/SRPMS
    cp deploy/RPM-GPG-KEY-MDSplus /buildroot/etc/pki/rpm-gpg/
    outfile=/buildroot/etc/yum.repos.d/mdsplus${BNAME}.repo
    echo [MDSplus${BNAME}] > $outfile
    echo "name=MDSplus${BNAME}" >> $outfile
    echo "baseurl=http://www.mdsplus.org/dist/${DIST}/${BRANCH}/RPMS" >> $outfile
    echo "enabled=1" >> $outfile
    cat - >> $outfile <<EOF
gpgcheck=1
gpgkey=file:///etc/pki/rpm-gpg/RPM-GPG-KEY-MDSplus
metadata_expire=300
EOF
    build_rpms_mdsplus
    #
    # If everything went successfully then make a marker file indicating
    # that an installer has been built for this release.
    #
    touch /installer/mdsplus${BNAME}-${MAJOR}.${MINOR}-${RELEASE}
fi

if [ -d /EGGS ]
then
    #
    # If an /EGGS directory is available build python distribution eggs
    # which will be used by easy_install.
    #
    build_eggs
fi

    
